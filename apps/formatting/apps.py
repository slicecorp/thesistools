from django.apps import AppConfig


class FormattingConfig(AppConfig):
    name = 'formatting'
