from django import forms
from .models import Thesis


class ThesisForm(forms.ModelForm):
    class Meta:
        model = Thesis
        fields = ('title', 'department', 'level', 'urgency', 'document', 'output_format', 'comments')
