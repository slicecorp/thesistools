# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-23 17:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('formatting', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='thesis',
            name='document',
            field=models.FileField(default='', upload_to='media/documents/%Y/%m/%d/'),
        ),
    ]
