Thesis Tools
=======================

This is a basic web app to 
1. convert thesis to and from required formats eg .docx <-> .tex,
2. analyze and generate reports for the data analysis chapter in thesis,
3. detect plagiarism in thesis primarily with an intrinsic detection scheme.


Meta
----

Author:
    Hisham Osman

Status:
    maintained, in development

Version:
    0.1

Django Version:
    1.10.6



Usage
-----

python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
unzip userena and overwrite files in <python env>/lib/python3.5/site-packages/userena

Documentation
-------------

